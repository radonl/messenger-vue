const jwt = require('jsonwebtoken');

const formatServicesResponse = require('../helpers/formatServicesResponse');
const formatResponse = require('../helpers/formatResponse');
const sessionRepository = require('../repositories/session.repository');
const userRepository = require('../repositories/user.repository');

const notAuthorized = res => formatResponse(formatServicesResponse.notAuthorized('Not authorized!'), res);

module.exports = async (req, res, next) => {
  try {
    const token = req.header('Authorization');
    if(token) {
      const session = await sessionRepository.get(token);
      if (session.isActive) {
        const user = await userRepository.get(session.userId);
        req.authData = {user, session};
        next();
      } else {
        notAuthorized(res);
      }
    } else {
      notAuthorized(res);
    }
  } catch (e) {
    notAuthorized(res);
  }
};