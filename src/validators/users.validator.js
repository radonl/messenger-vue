const { check, param } = require('express-validator');

module.exports = {
  sendVerificationCode: [
    check('phone').isLength({min: 12, max: 12})
      .withMessage('Phone should contain 12 symbols'),
    check('phone').matches(/^\d+$/)
      .withMessage('The phone can only contain numbers')
  ],
  login: [
    check('phone').isLength({min: 12, max: 12})
      .withMessage('Phone should contain 12 symbols'),
    check('phone').matches(/^\d+$/)
      .withMessage('Phone can only contain numbers'),
    check('code').isLength({min: 6, max: 6})
      .withMessage('Code should contain 6 symbols'),
    check('code').matches(/^\d+$/)
      .withMessage('Code can only contain numbers')
  ],
  get: [
    param('id').isNumeric()
      .withMessage('Id should be numeric')
  ],
  search: [
    param('q').isLength({max: 64, min: 1})
  ],
  updateProfile: [
    check('displayedName')
      .isLength({min: 1, max: 32})
      .withMessage('Displayed name should contain from 1 to 32 symbols'),
    check('username')
      .isLength({min: 6, max: 32})
      .withMessage('Name should contain from 6 to 32 symbols'),
    check('username')
      .matches(/^[a-zA-Z0-9_]*/)
      .withMessage('Username can contain only numbers, letters and _')
  ]
};