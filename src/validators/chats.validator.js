const { check, param } = require('express-validator');

module.exports = {
  create: [
    check('name').isLength({max: 64})
  ],
  get: [
    param('id').isNumeric()
      .withMessage('Id should be numeric')
  ],
  update: [
    param('id').isNumeric()
      .withMessage('Id should be numeric'),
    check('name').isString(),
    check('name').isLength({min: 1, max: 64})
  ],
  getMessages: [
    param('id').isNumeric()
      .withMessage('Id should be numeric'),
    check('offset')
      .isNumeric()
      .withMessage('Offset should be numeric'),
    check('limit')
      .isNumeric()
      .withMessage('Offset should be numeric')
  ],
  getMessagesCount: [
    param('id').isNumeric()
      .withMessage('Id should be numeric')
  ],
  sendMessage: [
    param('id').isNumeric()
      .withMessage('Id should be numeric'),
    check('message').isLength({max: 4096})
  ],
  sendFile: [
    param('id').isNumeric()
      .withMessage('Id should be numeric'),
  ],
  writingMessage: [
    param('id').isNumeric()
      .withMessage('Id should be numeric')
  ],
  addUserToGroup: [
    param('id').isNumeric()
      .withMessage('id should be numeric'),
    param('userId').isNumeric()
      .withMessage('userId should be numeric'),
  ],
  removeUserFromGroup: [
    param('id').isNumeric()
      .withMessage('id should be numeric'),
    param('userId').isNumeric()
      .withMessage('userId should be numeric'),
  ],
  removeChat: [
    param('id').isNumeric()
      .withMessage('Id should be numeric')
  ],
  clearChat: [
    param('id').isNumeric()
      .withMessage('Id should be numeric')
  ]
};