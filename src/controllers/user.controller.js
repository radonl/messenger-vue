const userService = require('../services/user.service');
const formatResponse = require('../helpers/formatResponse');
const validate = require('../helpers/validate');

class UserController {
  async login(req, res) {
    if(validate(req, res)) {
      formatResponse(await userService.login({
        ...req.body,
        ip: req.headers['x-forwarded-for'] || req.connection.remoteAddress
      }), res);
    }
  }
  async sendVerificationCode (req, res) {
    if (validate(req, res)) {
      formatResponse(await userService.sendVerificationCode(req.body), res);
    }
  }

  async get(req, res) {
    if(validate(req, res)) {
      formatResponse(await userService.get(req.params.id), res);
    }
  }

  async search(req, res) {
    if(validate(res, req)) {
      formatResponse(await userService.search(req.params.q), res);
    }
  }

  async updateProfile(req, res) {
    if(validate(res, req)) {
      formatResponse(await userService.updateProfile(req.body, req.authData.user), res);
    }
  }

  async logout(req, res) {
    if(validate(res, req)) {
      formatResponse(await userService.logout(req.authData.session), res)
    }
  }
}

module.exports = new UserController();