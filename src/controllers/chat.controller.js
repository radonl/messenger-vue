const chatService = require('../services/chat.service');
const formatResponse = require('../helpers/formatResponse');
const validate = require('../helpers/validate');

class ChatController {
  async get(req, res) {
    if(validate(req, res)) {
      formatResponse(await chatService.get(req.params.id, req.authData.user), res);
    }
  }

  async update(req, res) {
    if(validate(req, res)) {
      formatResponse(await chatService.update(req.params.id, req.body, req.authData.user), res);
    }
  }

  async create(req, res) {
    if (validate(req, res)) {
      formatResponse(await chatService.create(req.body, req.authData.user), res);
    }
  }

  async getAll(req, res) {
    if(validate(req, res)) {
      formatResponse(await chatService.getAll(req.authData.user), res);
    }
  }

  async getMessages(req, res) {
    if(validate(req, res)) {
      formatResponse(await chatService.getMessages(req.params.id, req.query, req.authData.user), res);
    }
  }

  async getMessagesCount(req, res) {
    if(validate(req, res)) {
      formatResponse(await chatService.getMessagesCount(req.params.id, req.authData.user), res);
    }
  }

  async sentMessage(req, res) {
    if(validate(req, res)) {
      formatResponse(await chatService.sentMessage(req.params.id, req.body, req.authData.user), res);
    }
  }

  async sendFile(req, res) {
    if(validate(req, res)) {
      formatResponse(await chatService.sendFile(req.params.id, req.body, req.authData.user), res);
    }
  }

  async writingMessage(req, res) {
    if(validate(req, res)) {
      formatResponse(await chatService.writingMessage(req.params.id, req.authData.user), res);
    }
  }

  async addUser(req, res) {
    if(validate(req, res)) {
      formatResponse(await chatService.addUser(req.params.id, req.params.userId, req.authData.user), res);
    }
  }

  async removeUser(req, res) {
    if(validate(req, res)){
      formatResponse(await chatService.removeUser(req.params.id, req.params.userId, req.authData.user), res);
    }
  }

  async removeChat(req, res) {
    if(validate(req, res)) {
      formatResponse(await chatService.removeChat(req.params.id, req.authData.user), res);
    }
  }

  async clearChat(req, res) {
    if(validate(req, res)) {
      formatResponse(await chatService.clearChat(req.params.id, req.authData.user), res);
    }
  }
}

module.exports = new ChatController();