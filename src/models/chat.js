'use strict';
module.exports = (sequelize, DataTypes) => {
  const Chat = sequelize.define('Chat', {
    name: DataTypes.STRING,
    isPrivate: DataTypes.BOOLEAN,
    avatarPath: DataTypes.STRING,
    ownerId: DataTypes.INTEGER
  }, {});
  Chat.associate = function(models) {
    models.Chat.belongsToMany(models.User, {
      through: models.ChatMember,
      foreignKey: 'chatId',
      otherKey: 'userId',
      as: 'members'
    });
    models.Chat.hasMany(models.Message, {
      foreignKey: 'chatId',
      as: 'messages'
    });
  };
  return Chat;
};