'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    displayedName: DataTypes.STRING,
    phone: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false
    },
    username: DataTypes.STRING,
    avatarPath: DataTypes.STRING
  }, {});
  User.associate = function(models) {
    User.hasMany(models.Session, {
      foreignKey: 'userId',
    })
  };
  return User;
};