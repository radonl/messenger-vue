'use strict';
module.exports = (sequelize, DataTypes) => {
  const Session = sequelize.define('Session', {
    token: DataTypes.STRING,
    isActive: DataTypes.BOOLEAN,
    ip: DataTypes.STRING,
    userId: DataTypes.INTEGER
  }, {});
  Session.associate = function(models) {

  };
  return Session;
};