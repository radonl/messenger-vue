'use strict';
module.exports = (sequelize, DataTypes) => {
  const File = sequelize.define('File', {
    isMedia: DataTypes.BOOLEAN,
    path: DataTypes.STRING,
    messageId: DataTypes.INTEGER
  }, {});
  File.associate = function(models) {

  };
  return File;
};