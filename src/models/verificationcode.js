'use strict';
module.exports = (sequelize, DataTypes) => {
  const VerificationCode = sequelize.define('VerificationCode', {
    code: DataTypes.STRING,
    userId: DataTypes.INTEGER
  }, {});
  VerificationCode.associate = function (models) {

  };
  return VerificationCode;
};