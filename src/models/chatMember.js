'use strict';
module.exports = (sequelize, DataTypes) => {
  const Chat = sequelize.define('ChatMember', {
    chatId: DataTypes.INTEGER,
    userId: DataTypes.INTEGER
  }, {});
  Chat.associate = function(models) {

  };
  return Chat;
};