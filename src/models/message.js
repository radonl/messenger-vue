'use strict';
module.exports = (sequelize, DataTypes) => {
  const Message = sequelize.define('Message', {
    message: DataTypes.TEXT,
    senderId: DataTypes.INTEGER,
    chatId: DataTypes.INTEGER
  }, {});
  Message.associate = function(models) {

  };
  return Message;
};