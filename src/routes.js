const express = require('express');
const router = express.Router();

const usersController = require('./controllers/user.controller');
const chatsController = require('./controllers/chat.controller');
const usersValidator = require('./validators/users.validator');
const chatsValidator = require('./validators/chats.validator');
const auth = require('./middlewares/auth');

router.post('/sendVerificationCode', usersValidator.sendVerificationCode, usersController.sendVerificationCode);
router.post('/login', usersValidator.login, usersController.login);
router.post('/logout', auth, usersController.logout);
router.get('/users/:id', [...usersValidator.get, auth], usersController.get);
router.get('/users/search/:q', [...usersValidator.search, auth], usersController.search);
router.put('/users', auth, usersController.updateProfile);

router.get('/chats', auth, chatsController.getAll);
router.post('/chats', [...chatsValidator.create, auth], chatsController.create);
router.get('/chats/:id', [...chatsValidator.get, auth], chatsController.get);
router.put('/chats/:id', [...chatsValidator.update, auth], chatsController.update);
router.get('/chats/:id/messages', [...chatsValidator.getMessages, auth], chatsController.getMessages);
router.get('/chats/:id/messages/count', [...chatsValidator.getMessagesCount, auth], chatsController.getMessagesCount);
router.post('/chats/:id/messages', [...chatsValidator.sendMessage, auth], chatsController.sentMessage);
router.post('/chats/:id/messages/file',[...chatsValidator.sendFile, auth],chatsController.sendFile);
router.get('/chats/:id/writing-message', [...chatsValidator.writingMessage, auth], chatsController.writingMessage);

router.post('/chats/:id/users/:userId', [...chatsValidator.addUserToGroup, auth], chatsController.addUser)
router.delete('/chats/:id/users/:userId', [...chatsValidator.removeUserFromGroup, auth], chatsController.removeUser)

router.delete('/chats/:id', [...chatsValidator.removeChat, auth], chatsController.removeChat);
router.delete('/chats/:id/clear', [...chatsValidator.clearChat, auth], chatsController.clearChat)


module.exports = router;
