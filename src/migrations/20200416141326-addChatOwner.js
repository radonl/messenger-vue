'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return new Promise(async (resolve, reject) => {
      try {
        await queryInterface.addColumn('Chats', 'ownerId', {
          type: Sequelize.INTEGER,
          allowNull: true
        });
        await queryInterface.addConstraint('Chats', ['ownerId'], {
          type: 'foreign key',
          references: {
            table: 'Users',
            field: 'id'
          },
          onDelete: 'cascade',
          onUpdate: 'cascade'
        });
        resolve();
      } catch (e) {
        reject(e);
      }
    })
  },

  down: (queryInterface, Sequelize) => {
    return new Promise(async (resolve, reject) => {
      try {
        await queryInterface.removeConstraint('Chats', 'Chats_ownerId_Users_fk');
        await queryInterface.removeColumn('Chats', 'ownerId');
        resolve();
      } catch (e) {
        reject(e);
      }
    });
  }
};
