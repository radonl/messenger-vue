'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return new Promise(async (resolve, reject) => {
      try {
        await queryInterface.removeConstraint('Messages', 'Messages_recipientId_fkey');
        await queryInterface.removeColumn('Messages', 'recipientId');
        await queryInterface.addColumn('Messages', 'chatId', Sequelize.INTEGER);
        await queryInterface.addConstraint('Messages', ['chatId'], {
          type: 'foreign key',
          references: {
            table: 'Chats',
            field: 'id'
          },
          onDelete: 'cascade',
          onUpdate: 'cascade'
        });
        resolve();
      } catch (e) {
        reject(e);
      }
    })
  },

  down: (queryInterface, Sequelize) => {
    return new Promise(async (resolve, reject) => {
      try {
        await queryInterface.addColumn('Messages', 'recipientId', Sequelize.INTEGER);
        await queryInterface.addConstraint('Messages', ['recipientId'], {
          references: {
            table: 'Users',
            field: 'id'
          },
          onDelete: 'cascade',
          onUpdate: 'cascade'
        });
        await queryInterface.removeConstraint('Messages', 'Messages_chatId_Chats_fk');
        await queryInterface.removeColumn('Messages', 'chatId');
        resolve();
      } catch (e) {
        reject(e);
      }
    });
  }
};
