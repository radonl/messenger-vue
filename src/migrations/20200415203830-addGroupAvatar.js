'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return new Promise(async (resolve, reject) => {
      await queryInterface.addColumn('Chats', 'avatarPath', Sequelize.STRING);
      resolve();
    })
  },

  down: (queryInterface, Sequelize) => {
    return new Promise(async (resolve, reject) => {
      await queryInterface.removeColumn('Chats', 'avatarPath');
      resolve();
    })
  }
};
