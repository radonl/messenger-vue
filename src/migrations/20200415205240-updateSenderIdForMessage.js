'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return new Promise(async (resolve, reject) => {
      await queryInterface.changeColumn('Messages', 'senderId', {
        type: Sequelize.INTEGER,
        allowNull: true,
      });
      resolve();
    })
  },

  down: (queryInterface, Sequelize) => {
    return new Promise(async (resolve, reject) => {
      await queryInterface.removeColumn('Messages', 'senderId', {
        type: Sequelize.INTEGER,
        onDelete: "CASCADE",
        allowNull: false,
        references: {
          model: 'Users',
          key: 'id'
        }
      });
      resolve();
    })
  }
};
