const models = require('../models');

class SessionRepository {
  async create(user, token, ip) {
    return await models.Session.create({
      isActive: true,
      userId: user.id,
      ip,
      token
    })
  }

  async get(token) {
    return models.Session.findOne({
      where: {
        token
      }
    });
  }

  async deactivateSession(session) {
    session.isActive = false;
    session.save();
  }
}

module.exports = new SessionRepository();