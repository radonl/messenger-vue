const {Sequelize} = require('sequelize');

const models = require('../models');

class UserRepository {
  async getOrCreate(phone) {
    return await models.User.findOne({
      where: {phone}
    }) || await models.User.create({
      phone
    })
  }

  async get(id) {
    return models.User.findOne({
      where: {id}
    });
  }

  async getByPhone(phone) {
    return await models.User.findOne({
      where: {
        phone
      }
    });
  }

  async getByUserName(username) {
    return await models.User.findOne({
      where: {
        username
      }
    });
  }

  async search(q) {
    if (q.length < 4) {
      return [];
    }
    return await models.User.findAll({
      limit: 10,
      where: {
        [Sequelize.Op.or]: [{
          phone: {
            [Sequelize.Op.eq]: q
          }
        }, {
          username: {
            [Sequelize.Op.like]: `%${q}%`
          }
        }]

      }
    })
  }

  async updateDisplayedName(displayedName, user) {
    user.displayedName = displayedName;
    return await user.save();
  }

  async updateUsername(username, user) {
    user.username = username;
    return await user.save();
  }

  async updateAvatar(avatarPath, user) {
    user.avatarPath = avatarPath;
    return await user.save();
  }
}

module.exports = new UserRepository();