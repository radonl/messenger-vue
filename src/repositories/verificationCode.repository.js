const models = require('../models');

class VerificationCodeRepository {
  async create(user, code) {
    return await models.VerificationCode.create({
      userId: user.id,
      code
    });
  }

  async get(user, code) {
    return await models.VerificationCode.findOne({
      where: {
        userId: user.id,
        code
      }
    })
  }
}

module.exports = new VerificationCodeRepository();