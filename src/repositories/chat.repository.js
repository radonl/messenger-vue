const {Sequelize} = require('sequelize');

const models = require('../models');

class ChatRepository {
  async get(id) {
    return models.Chat.findOne({
      where: {id},
      include: {
        model: models.User,
        as: 'members'
      },
    });
  }

  getChatWithMembersTokens(id) {
    return models.Chat.findOne({
      where: {id},
      include: {
        model: models.User,
        as: 'members',
        include: {
          model: models.Session
        }
      },
    });
  }

  async create(users, name, ownerId) {
    const isPrivate = users.length === 2;
    if (isPrivate) {
      let chats = await models.Chat.findAll({
        include: {
          model: models.User,
          as: 'members'
        },
        where: {
          id: await models.ChatMember.findAll({
            where: {
              [Sequelize.Op.or]: users.map(userId => ({
                userId: {
                  [Sequelize.Op.eq]: userId
                }
              }))
            }
          }).map(cm => cm.chatId),
          isPrivate: true
        }
      });
      chats = chats.filter(chat => {
        return chat.members.find(m => m.id === users[0]) && chat.members.find(m => m.id === users[1]);
      });
      if (chats.length === 1) {
        return chats[0];
      }
    }
    const chat = await models.Chat.create({
      isPrivate,
      name,
      ...isPrivate ? {} : {ownerId}
    });
    await models.ChatMember.bulkCreate(users.map(userId => ({
      chatId: chat.id,
      userId
    })));
    return models.Chat.findOne({
      where: {id: chat.id},
      include: {
        model: models.User,
        as: 'members'
      },
    });
  }

  async getAll(user) {
    return models.Chat.findAll({
      include: [{
        model: models.User,
        as: 'members'
      }, {
        model: models.Message,
        as: 'messages',
        limit: 1,
        order: [['updatedAt', 'DESC']]
      }],
      where: {
        id: await models.ChatMember.findAll({
          where: {
            userId: {
              [Sequelize.Op.eq]: user.id
            }
          }
        }).map(cm => cm.chatId)
      }
    });
  }

  getMessages(chatId, offset, limit) {
    return models.Message.findAll(({
      where: {
        chatId
      },
      order: [
        ['createdAt', 'ASC']
      ],
      offset,
      limit
    }))
  }

  async getMessagesCount(chatId) {
    return await models.Message.count(({
      where: {
        chatId
      }
    }))
  }

  async sentMessage(user, chatId, message) {
    return await models.Message.create({
      message,
      chatId,
      senderId: user.id
    })
  }

  async sendFile(user, chatId, filePath) {
    const message = await models.Message.create({
      message: '',
      chatId,
      senderId: user.id
    });
    return {
      ...message.dataValues,
      file: await models.File.create({
        isMedia: true,
        path: filePath,
        messageId: message.id
      })
    };
  }

  async addUser(chatId, userId) {
    await models.ChatMember.create({
      chatId,
      userId
    });
    return this.get(chatId)
  }

  async removeUser(chatId, userId) {
    const chatMember = await models.ChatMember.findOne({
      where: {
        chatId,
        userId
      }
    });
    await chatMember.destroy();
    return this.get(chatId)
  }

  async removeChat(chat) {
    return chat.destroy();
  }

  async clearChat(chatId) {
    return models.Message.destroy({
      where: {
       chatId
      }
    });
  }

  async updateGroup (id, name, avatarPath) {
    const group = await models.Chat.findOne({
      where: {
        id
      }
    });
    group.name = name;
    group.avatarPath = avatarPath;
    await group.save();
    return this.get(id);
  }

  async getFiles(messagesIds) {
    return models.File.findAll({
      where: {
        id: messagesIds
      }
    })
  }
}

module.exports = new ChatRepository();