const formatServicesResponse = require('../helpers/formatServicesResponse');
const chatRepository = require('../repositories/chat.repository');
const userRepository = require('../repositories/user.repository');
const socketService = require('../services/socketsService');
const storage = require('../services/storage');

class ChatService {
  async get(id, user) {
    try {
      const chat = await chatRepository.get(id);
      if (chat && chat.members.find(m => m.id === user.id)) {
        return formatServicesResponse.success(chat);
      }
      return formatServicesResponse.notFound('Chat');
    } catch (e) {
      return formatServicesResponse.exception(e);
    }
  }

  async update(id, {name, avatar}, user) {
    try {
      const chat = await chatRepository.get(id);
      if (!chat) {
        return formatServicesResponse.notFound('Chat');
      }
      if (chat.ownerId !== user.id) {
        return formatServicesResponse.notHavePermission('edit', 'chat');
      }
      if (name !== chat.name) {
        this.sendSystemMessage(chat, `Group name changed to ${name}`)
      }
      if (avatar) {
        chat.avatarPath = await storage.createFile(avatar.fileExtension, avatar.fileContent, 'groups');
        this.sendSystemMessage(chat, `Group ${name} avatar changed`);
      }
      const updatedGroup = await chatRepository.updateGroup(id, name, chat.avatarPath);
      await this.sendGroupUpdatedEvent(updatedGroup);
      return formatServicesResponse.success(updatedGroup);
    } catch (e) {
      return formatServicesResponse.exception(e);
    }
  }

  async create({users, name}, user) {
    try {
      users.push(user.id);
      const chat = await chatRepository.create(users, name, user.id);
      socketService.connection().sendEvent(
        'YOU_ADDED_TO_GROUP',
        chat,
        await this.getMembersTokens(chat.id)
      );
      if (!chat.isPrivate) {
        const timeout = setTimeout(async () => {
          socketService.connection().sendEvent(
            'INCOMING_MESSAGE',
            await chatRepository.sentMessage({}, chat.id, 'Group created!'),
            await this.getMembersTokens(chat.id)
          );
          clearTimeout(timeout);
        }, 1000)
      }
      return formatServicesResponse.success(chat);
    } catch (e) {
      return formatServicesResponse.exception(e);
    }
  }

  async getAll(user) {
    try {
      return formatServicesResponse.success(await chatRepository.getAll(user));
    } catch (e) {
      return formatServicesResponse.exception(e);
    }
  }

  async getMessages(chatId, {offset, limit}, user) {
    try {
      const messages = await chatRepository.getMessages(chatId, offset, limit);
      const files = await chatRepository.getFiles(messages.map(m => m.id));
      return formatServicesResponse.success(messages.map(m => {
        const file = files.find(f => f.messageId === m.id);
        return {
          ...m.dataValues,
          file
        }
      }));
    } catch (e) {
      return formatServicesResponse.exception(e);
    }
  }

  async getMessagesCount(chatId, user) {
    try {
      return formatServicesResponse.success(await chatRepository.getMessagesCount(chatId))
    } catch (e) {
      return formatServicesResponse.exception(e);
    }
  }

  async getMembersTokens(chatId) {
    const chat = await chatRepository.getChatWithMembersTokens(chatId);
    if (chat && chat.members) {
      return chat.members.reduce((tokens, member) => {
        if (member.Sessions) {
          tokens.push(...member.Sessions.map(session => session.token));
        }
        return tokens;
      }, []);
    }
    return [];
  }

  async sentMessage(chatId, {message}, user) {
    try {
      const result = await chatRepository.sentMessage(user, chatId, message);
      socketService.connection().sendEvent('INCOMING_MESSAGE', result, await this.getMembersTokens(chatId));
      return formatServicesResponse.success(result);
    } catch (e) {
      return formatServicesResponse.exception(e);
    }
  }

  async sendFile(chatId, {fileExtension, fileContent}, user) {
    try {
      const filePath = await storage.createFile(fileExtension, fileContent, 'files');
      const result = await chatRepository.sendFile(user, chatId, filePath);
      socketService.connection().sendEvent('INCOMING_MESSAGE', result, await this.getMembersTokens(chatId));
      return formatServicesResponse.success(result);
    } catch (e) {
      return formatServicesResponse.exception(e)
    }
  }

  async writingMessage(chatId, {id}) {
    try {
      socketService.connection().sendEvent('USER_WRITES_MESSAGE', {
        userId: id,
        chatId: Number.parseInt(chatId)
      }, await this.getMembersTokens(chatId));
      return formatServicesResponse.success({});
    } catch (e) {
      return formatServicesResponse.exception(e);
    }
  }

  async addUser(chatId, userId, user) {
    try {
      const chat = await chatRepository.get(chatId);
      if (!chat) {
        return formatServicesResponse.notFound('Chat');
      }
      if (chat.ownerId !== user.id) {
        return formatServicesResponse.notHavePermission('edit', 'chat');
      }
      const memberIsExist = !!chat.members.find(member => member.id === Number.parseInt(userId));
      if (memberIsExist) {
        return formatServicesResponse.conflict('Chat', 'Members');
      }
      const dbUser = userRepository.get(userId);
      if (!dbUser) {
        return formatServicesResponse.notFound('User');
      }
      const group = await chatRepository.addUser(Number.parseInt(chat.id), Number.parseInt(userId));
      this.sendSystemMessage(group, `User ${dbUser.phone} added to ${chat.name}`);
      await this.sendGroupUpdatedEvent(group);
      return formatServicesResponse.success(group);
    } catch (e) {
      return formatServicesResponse.exception(e);
    }
  }

  async removeUser(chatId, userId, user) {
    try {
      const chat = await chatRepository.get(chatId);
      if (!chat) {
        return formatServicesResponse.notFound('Chat');
      }
      if (chat.ownerId !== user.id) {
        return formatServicesResponse.notHavePermission('edit', 'chat');
      }
      const member = chat.members.find(member => member.id === Number.parseInt(userId));
      if (!member) {
        return formatServicesResponse.notFound('Member');
      }
      const updatedChat = await chatRepository.removeUser(chatId, userId);
      await this.sendGroupUpdatedEvent(updatedChat);
      this.sendSystemMessage(chat, `User ${user.phone} removed`);
      return formatServicesResponse.success(updatedChat);
    } catch (e) {
      return formatServicesResponse.exception(e);
    }
  }

  async removeChat(chatId, user) {
    try {
      const chat = await chatRepository.get(chatId);
      if (!chat) {
        return formatServicesResponse.notFound('Chat');
      }
      if (chat.ownerId !== user.id) {
        return formatServicesResponse.notHavePermission('edit', 'chat');
      }
      const result = await chatRepository.removeChat(chat);
      await this.sendGroupRemovedEvent(chat);
      return formatServicesResponse.success(result);
    } catch (e) {
      return formatServicesResponse.exception(e);
    }
  }

  async clearChat(chatId, user) {
    try {
      const chat = await chatRepository.get(chatId);
      if (!chat) {
        return formatServicesResponse.notFound('Chat');
      }
      if (chat.ownerId !== user.id) {
        return formatServicesResponse.notHavePermission('edit', 'chat');
      }
      await chatRepository.clearChat(chat.id);
      socketService.connection().sendEvent(
        'CLEAR_CHAT',
        chatId,
        await this.getMembersTokens(chat.id)
      );
      socketService.connection().sendEvent(
        'INCOMING_MESSAGE',
        await chatRepository.sentMessage({}, chat.id, 'Chat is cleared!'),
        await this.getMembersTokens(chat.id)
      );
      return formatServicesResponse.success();
    } catch (e) {
      return formatServicesResponse.exception(e);
    }
  }

  async sendGroupRemovedEvent(chat) {
    socketService.connection().sendEvent(
      'GROUP_REMOVED',
      chat,
      await this.getMembersTokens(chat.id)
    );
  }

  async sendGroupUpdatedEvent(chat) {
    socketService.connection().sendEvent(
      'GROUP_UPDATED',
      chat,
      await this.getMembersTokens(chat.id)
    );
  }

  sendSystemMessage(chat, message) {
    const timeout = setTimeout(async () => {
      socketService.connection().sendEvent(
        'INCOMING_MESSAGE',
        await chatRepository.sentMessage({}, chat.id, message),
        await this.getMembersTokens(chat.id)
      );
      clearTimeout(timeout);
    }, 1000);
  }
}

module.exports = new ChatService();