const jwt = require('jsonwebtoken');
const axios = require('axios/index');

const userRepository = require('../repositories/user.repository');
const verificationCodeRepository = require('../repositories/verificationCode.repository');
const sessionRepository = require('../repositories/session.repository');
const formatServicesResponse = require('../helpers/formatServicesResponse');
const storage = require('./storage');

class UserService {
  async login({phone, code, ip}) {
    try {
      const user = await userRepository.getByPhone(phone);
      if (user) {
        const verificationCode = await verificationCodeRepository.get(user, code);
        if (verificationCode) {
          return formatServicesResponse.success({
            session: await sessionRepository.create(
              user,
              jwt.sign({id: user.id}, process.env.JWT_KEY, {
                  expiresIn: '1d'
              }),
              ip
            ),
            user
          });
        }
        return formatServicesResponse.notFound('Code');
      }
      return formatServicesResponse.notFound('User');
    } catch (e) {
      return formatServicesResponse.exception(e);
    }
  }

  async logout (session) {
    try {
      await sessionRepository.deactivateSession(session)
      return formatServicesResponse.success();
    } catch (e) {
      return formatServicesResponse.exception(e);
    }
  }

  sendVerificationCode({phone}) {
    return new Promise(async (resolve) => {
      try {
        const user = await userRepository.getOrCreate(phone);
        const code = await verificationCodeRepository.create(user, Math.round(99999.5 + Math.random() * 900000));
        // axios.get(process.env.SEMYSMS_API_URL, {
        //   params: {
        //     device: process.env.SENYSMS_DEVICE_ID,
        //     token: process.env.SEMYSMS_TOKEN,
        //     msg: `Your verification code: ${code.code}`,
        //     phone
        //   }
        // }).then(res => {
        //   resolve(formatServicesResponse.success({
        //     user,
        //     sms: res.data
        //   }));
        // }).catch(e => {
        //   resolve(formatServicesResponse.exception(e));
        // });
        resolve(formatServicesResponse.success({
          user,
          sms: {}
        }));
      } catch (e) {
        resolve(formatServicesResponse.exception(e));
      }
    });
  }

  async get(id) {
    try {
      const user = await userRepository.get(id);
      if (user) {
        return formatServicesResponse.success({user});
      }
      return formatServicesResponse.notFound('User');
    } catch (e) {
      return formatServicesResponse.exception(e);
    }
  }

  async search(q) {
    try {
      return formatServicesResponse.success(await userRepository.search(q));
    } catch (e) {
      return formatServicesResponse.exception(e);
    }
  }

  async updateProfile({displayedName, avatar, username}, user) {
    try {
      if(username) {
        const dbUser = await userRepository.getByUserName(username);
        if (dbUser && dbUser.id !== user.id) {
          return formatServicesResponse.conflict('User', 'username');
        }
        return formatServicesResponse.success(await userRepository.updateUsername(username, user));
      }
      if(displayedName) {
        return formatServicesResponse.success(await userRepository.updateDisplayedName(displayedName, user));
      }
      if(avatar) {
        const avatarPath = await storage.createFile(avatar.fileExtension, avatar.fileContent, 'users');
        if(avatarPath) {
          return formatServicesResponse.success(await userRepository.updateAvatar(avatarPath, user));
        }
      }
      return formatServicesResponse.notFound('User');
    } catch (e) {
      return formatServicesResponse.exception(e);
    }
  }
}

module.exports = new UserService();