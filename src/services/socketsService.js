let connection = null;

const sessionRepository = require('../repositories/session.repository');

class SocketsService {
  constructor() {
    this.sockets = new Map();
  }

  connect(server) {
    const io = require('socket.io')(server);
    io.on('connection', (socket) => {
      socket.on('Authorization', async (data) => {
        try {
          if (data.token) {
            const session = await sessionRepository.get(data.token);
            if (session.isActive) {
              this.sockets.set(data.token, socket);
            }
          }
        } catch (e) {
          console.log(e);
        }
      });
      socket.on('disconnect', () => {
        console.log(socket.id, "Disconnect");
        this.sockets.delete(socket.id);
      });
      console.log(`New socket connection: ${socket.id}`);
    });
  }

  sendEvent(event, data, tokens) {
    if(tokens) {
      tokens.forEach(token => {
        const socket = this.sockets.get(token);
        if (socket) {
          socket.emit(event, data);
        }
      })
    }
  }

  registerEvent(event, handler) {
    [...this.sockets.keys()].forEach(key => this.sockets.get(key).on(event, handler));
  }

  static init(server, sessionMiddleware) {
    if (!connection) {
      connection = new SocketsService();
      connection.connect(server, sessionMiddleware);
    }
  }

  static getConnection() {
    if (!connection) {
      throw new Error("no active connection");
    }
    return connection;
  }
}

module.exports = {
  connect: SocketsService.init,
  connection: SocketsService.getConnection
};
