const { validationResult } = require('express-validator');

const formatResponse = require('../helpers/formatResponse');
const formatServicesResponse = require('../helpers/formatServicesResponse');

module.exports = (req,res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    formatResponse(
      formatServicesResponse.unprocessableEntity(errors),
      res
    );
    return false;
  }
  return true;
};