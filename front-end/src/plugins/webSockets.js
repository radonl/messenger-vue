import io from 'socket.io-client';

const socket = io.connect('http://localhost:3000', {});

socket.emit('Authorization', JSON.parse(localStorage.getItem('session')));

export default function webSockets() {
  return store => {
    socket.on('INCOMING_MESSAGE', data => store.dispatch('messages/incomingMessage', data));
    socket.on('USER_WRITES_MESSAGE', data => store.dispatch('messages/userWritesMessage', data));
  }
}