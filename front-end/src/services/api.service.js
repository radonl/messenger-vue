const apiUrl = process.env.VUE_APP_API_URL;

class Api {
  async _call({ url, method = "GET", body = null }) {
    const session = JSON.parse(localStorage.getItem("session"));
    const options = {
      method,
      headers: {
        "Content-Type": "application/json",
        ...session ? { Authorization: session.token } : {}
      }
    };

    if (body) {
      options.body = JSON.stringify(body);
    }

    try {
      const response = await fetch(`${apiUrl}/${url}`, options);
      const responseBody = await response.json();

      if (!response.ok) {
        return Promise.reject({
          body: responseBody,
          message: responseBody.message,
          status: response.status
        });
      }

      return responseBody;
    } catch (err) {
      throw new {
        body: err.message,
        status: 500,
        message: err.message
      };
    }
  }

  get(url) {
    return this._call({ url });
  }

  post(url, body) {
    return this._call({ url, method: "POST", body });
  }

  put(url, body) {
    return this._call({ url, method: "PUT", body });
  }

  delete(url, body){
    return this._call({ url, method: "DELETE", body });
  }
}

export default new Api();
