import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import VueMaterial from 'vue-material'
import axios from 'axios'
import VueAxios from 'vue-axios'
import './assets/css/tailwind.css'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'

Vue.config.productionTip = false;


Vue.use(VueMaterial);

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app');
