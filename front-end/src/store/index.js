import Vue from 'vue'
import Vuex from 'vuex'
import login from './modules/login';
import icons from "./modules/icons";
import session from "./modules/session";
import user from "./modules/user";
import chats from "./modules/chats";
import messages from "./modules/messages";
import users from "./modules/users";
import search from "./modules/search";
import webSockets from "../plugins/webSockets";

Vue.use(Vuex);

export default new Vuex.Store({
  actions: {},
  mutations: {},
  state: {},
  getters: {},
  modules: {
    login,
    icons,
    session,
    user,
    chats,
    messages,
    users,
    search
  },
  plugins: [webSockets()]
})
