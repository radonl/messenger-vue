import ApiService from '../../services/api.service';

export default {
  namespaced: true,
  actions: {
    loadChats ({commit, dispatch}) {
      ApiService.get('chats').then(res => {
        if(res.status) {
          commit('updateChats', res.data);
          dispatch('users/checkAndUpdateUsers', res.data, {root: true});
        }
      }).catch(e => console.error(e));
    },
    selectChat ({commit, state, dispatch}, id) {
      commit('updateChats', state.chats.map(chat => {
        chat.selected = chat.id === id;
        return chat;
      }));
      dispatch('messages/loadMessages', id, {root: true});
    },
    createChat({commit, state, dispatch}, partner) {
      const chat = {
        ...partner,
        isNewChat: true,
        id: 'New',
        isPrivate: true,
        selected: true,
        partnerId: partner.id,
        isSearchResult: false
      };
      const chats = state.chats.map(chat => {
        chat.selected = false;
        return chat;
      });
      chats.push(chat);
      commit('updateChats', chats);
      dispatch("search/chatPreparedToCreate", true, {root: true});
      // const chats = state.chats;
      // ApiService.post('chats', {
      //   users: [partner.id]
      // }).then(res => {
      //   chats.push({
      //     displayedName: partner.name,
      //     avatarPath: partner.avatarPath,
      //     id: res.data.id
      //   });
      //   commit('updateChats', chats);
      // }).catch(e => console.error(e));
    },
    chatCreated({state, commit}, chat) {
      debugger;
    }
  },
  mutations: {
    updateChats(state, chats) {
      state.chats = chats;
    }
  },
  state: {
    chats: []
  },
  getters: {
    chats(state, getters, rootState, rootGetters) {
      const user = rootGetters['user/me'];
      return state.chats.map(chat => {
        if(chat.isPrivate) {
          if(chat.isNewChat) {
            chat.name = chat.displayedName || chat.username || chat.phone;
          } else {
            const chatMember = chat.members.find(member => member.id !== user.id);
            chat.name = chatMember.displayedName || chatMember.username || chatMember.phone;
            chat.avatar = chatMember.avatarPath;
          }
        }
        return chat;
      });
    },
    selectedChatId(state) {
      const selectedChat = state.chats.find(chat => chat.selected);
      if(selectedChat) {
        return selectedChat.id;
      }
      return false;
    },
    selectedChatAvatar(state, getters, rootState, rootGetters) {
      const user = rootGetters['user/me'];
      const selectedChat = state.chats.find(chat => chat.selected);
      if(!selectedChat || !user) {
        return null;
      }
      if(selectedChat.isPrivate && !selectedChat.isNewChat) {
        const partner  = selectedChat.members.find(member => member.id !== user.id);
        return partner.avatarPath;
      }
      return selectedChat.avatarPath;
    },
    selectedChatPartnerId(state) {
      const selectedChat = state.chats.find(chat => chat.selected);
      return selectedChat.partnerId;
    }
  },
}