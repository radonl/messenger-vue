import ApiService from '../../services/api.service';

export default {
  namespaced: true,
  actions: {
    sendVerificationCode({commit, state}) {
      ApiService.post('sendVerificationCode', {
        phone: `380${state.phone}`
      }).then(() => {
          commit('updateSMSSended', true);
          commit('updateConfirmationModalIsOpen', false);
       }).catch(e => console.error(e));
    },
    next({commit, state}) {
      if(state.smsSended) {
        ApiService.post('login', {
          phone: `380${state.phone}`,
          code: state.code
        }).then(res => {
            if(res.status) {
              localStorage.setItem('session', JSON.stringify(res.data.session));
              localStorage.setItem('user', JSON.stringify(res.data.user));
              window.location.href = '/';
            }
        }).catch(error => {
          commit('updateError', error.response.data.error);
          console.log(error.response.data.error);
        });
      } else {
        commit('updateConfirmationModalIsOpen', true);
      }
    },
  },
  mutations: {
    updatePhone(state, event) {
      state.phone = event.target.value;
    },
    updateConfirmationModalIsOpen(state, status) {
      state.confirmationModalIsOpen = status;
    },
    updateSMSSended(state, status) {
      state.smsSended = status;
    },
    updateCode(state, code) {
      state.code = code;
      state.error = '';
    },
    updateError(state, error) {
      state.error = error;
    }
  },
  state: {
    phone: '',
    confirmationModalIsOpen: false,
    smsSended: false,
    code: '',
    error: ''
  },
  getters: {
    phone(state) {
      return state.phone;
    },
    code(state) {
      return state.code;
    },
    confirmationModalIsOpen(state) {
      return state.confirmationModalIsOpen;
    },
    smsSended(state) {
      return state.smsSended;
    },
    error(state) {
      return state.error;
    }
  },
}