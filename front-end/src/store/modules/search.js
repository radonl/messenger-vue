import ApiService from '../../services/api.service';

export default {
  namespaced: true,
  actions: {
    searchUsers({state, commit}, searchString) {
      if(searchString.length > 0) {
        commit('updateRequestInProgress', true);
        ApiService.get(`users/search/${searchString}`).then(res => {
          commit('updateResults', res.data.map(user => {
            user.name = user.displayedName || user.username || user.phone;
            user.avatar = user.avatarPath;
            user.isSearchResult = true;
            return user;
          }));
          commit('updateRequestInProgress', false);
          commit('updateDisplayResults', true);
        }).catch(e => console.error(e));
      } else {
        commit('updateDisplayResults', false);
      }
    },
    chatPreparedToCreate({state, commit}) {
      commit('updateDisplayResults', false);
    }
  },
  mutations: {
    updateResults(state, results) {
      state.results = results;
    },
    updateRequestInProgress(state, status) {
      state.requestInProgress = status;
    },
    updateDisplayResults(state, status) {
      state.displayResults = status;
    }
  },
  state: {
    results: [],
    requestInProgress: false,
    displayResults: false
  },
  getters: {
    results({results}) {
      return results;
    },
    requestInProgress({requestInProgress}) {
      return requestInProgress;
    },
    displayResults({displayResults}) {
      return displayResults;
    }
  }
}