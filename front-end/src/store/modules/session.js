import jwt_decode from 'jwt-decode';

export default {
  namespaced: true,
  actions: {},
  mutations: {},
  state: {
    session: JSON.parse(localStorage.getItem('session'))
  },
  getters: {
    isTokenExpired({session}) {
      if(session) {
        const token = session.token;
        if(token) {
          const decoded = jwt_decode(token);
          if (decoded.exp !== undefined) {
            const date = new Date(0);
            date.setUTCSeconds(decoded.exp);
            return !(date.valueOf() > new Date().valueOf())
          }
        }
      }
      return true;
    },
    session(state) {
      return state.session;
    }
  },
}