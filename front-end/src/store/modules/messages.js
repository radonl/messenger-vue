import ApiService from '../../services/api.service';
import moment from "moment";
import user from "./user";

export default {
  namespaced: true,
  actions: {
    loadMessages({commit, state}, id) {
      ApiService.get(`chats/${id}/messages?limit=10000&offset=0`).then(res => {
        commit('updateMessages', res.data.map(message => {
          message.displayedTime = moment(message.updatedAt).format('LTS');
          return message;
        }))
      }).catch(e => console.log(e));
    },
    sendMessage({commit, state}, {message, chatId}) {
      ApiService.post(`chats/${chatId}/messages`, {
        message: message
      }).then(res => console.info(res)).catch(e => console.error(e));
    },
    sendInitMessage({commit, state, dispatch}, {message, partnerId}) {debugger;
      ApiService.post('chats', {
        users: [partnerId]
      }).then(res => {
        dispatch('chats/chatCreated', res.data, {root: true});
      }).catch(e => console.error(e));
    },
    incomingMessage({commit, state}, message) {
      const messages = state.messages;
      messages.push(message);
      commit('updateMessages', messages);
    },
    userWritesMessage({state, rootGetters, commit, dispatch}, data) {
      if (rootGetters['user/me'].id !== data.userId) {
        const lastWritingMessage = moment();
        const user = rootGetters['users/user'](data.userId);
        dispatch('userWritesMessageAsyncUpdate', `${user.name} is typing...`);
        setTimeout(() => {
          if (lastWritingMessage.diff(state.lastWritingMessage, 'seconds') > 3) {
            dispatch('userWritesMessageAsyncUpdate', '');
          }
        }, 4000);
      }
    },
    userWritesMessageAsyncUpdate({commit}, text) {
      commit('updateUserWritesMessage', text);
    },
    meWritingMessage({commit, state}, selectedChatId) {
      const lastRequest = moment();
      if (lastRequest.diff(state.lastWritingMessageRequest, 'seconds') > 3) {
        commit('updateLastWritingMessageRequest', lastRequest);
        ApiService.get(`chats/${selectedChatId}/writing-message`).then(res => console.info(res)).catch(e => console.error(e))
      }
    }
  },
  mutations: {
    updateMessages(state, messages) {
      state.messages = messages;
    },
    updateLastWritingMessageRequest(state, now) {
      state.lastWritingMessageRequest = now;
    },
    updateUserWritesMessage(state, userWritesMessage) {
      state.userWritesMessage = userWritesMessage;
    }
  },
  state: {
    messages: [],
    userWritesMessage: '',
    lastWritingMessageRequest: moment(),
    lastWritingMessage: moment()
  },
  getters: {
    messages({messages}) {
      return messages;
    },
    newMessageText({newMessageText}) {
      return newMessageText;
    },
    userWritesMessage({userWritesMessage}) {
      return userWritesMessage;
    }
  }
}