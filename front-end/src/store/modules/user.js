import { mdiTelegram } from '@mdi/js';

export default {
  namespaced: true,
  actions: {},
  mutations: {},
  state: {
    user: JSON.parse(localStorage.getItem('user'))
  },
  getters: {
    me(state) {
      return state.user;
    }
  },
}