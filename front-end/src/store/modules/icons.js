import { mdiTelegram } from '@mdi/js';
import { mdiMenu } from '@mdi/js';
import { mdiMagnify } from '@mdi/js';

export default {
  namespaced: true,
  actions: {},
  mutations: {},
  state: {
    logo: mdiTelegram,
    menu: mdiMenu,
    search: mdiMagnify
  },
  getters: {
    logo(state) {
      return state.logo;
    },
    menu(state) {
      return state.menu
    },
    search(state) {
      return state.search;
    }
  },
}