export default {
  namespaced: true,
  actions: {
    checkAndUpdateUsers({commit, state}, chats) {
      const users = state.users;
      chats.forEach(chat => {
        chat.members.forEach(member => {
          if(!users.find(user => member.id === user.id)) {
            users.push(member);
          }
        })
      });
      commit('updateUsers', users);
    }
  },
  mutations: {
    updateUsers(state, users) {
      state.users = users;
    }
  },
  state: {
    users: []
  },
  getters: {
    users(state) {
      return state.users;
    },
    user(state) {
      return (id) => {
        const user = state.users.find(user => user.id === id);
        if(user) {
          return {
            avatar: user.avatarPath,
            name: user.displayedName || user.username || user.phone
          }
        } else {
          return null;
        }
      };
    }
  }
}