import Vue from 'vue'
import VueRouter from 'vue-router'

import LoginPage from '../pages/Login/LoginPage';
import Main from '../pages/Main/Main';

Vue.use(VueRouter);

const routes = [
  {
    path: '/login',
    name: 'LoginPage',
    component: LoginPage
  },
  {
    path: '/messages',
    name: 'Messages',
    component: Main
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router
